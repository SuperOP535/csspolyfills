const NOT_LIST_REGEX = /:not\((.*,.*)\)/g;

function polyfill(css) {
    return css.replace(NOT_LIST_REGEX, (full, selectors) => selectors.split(',').map(selector => ':not(' + selector + ')').join(''));
}