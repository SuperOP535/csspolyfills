# csspolyfills

CSS Polyfills designed to be used in bundlers/packagers or injected CSS.

## Explanation


### [:not](https://gitlab.com/SuperOP535/csspolyfills/blob/master/not_multiple.js)
- From [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:not): ```The ability to list more than one selector is experimental and not yet widely supported.```
- Converts :not(hello,world) to :not(hello):not(world) to extend support from Safari-only to every major browser.